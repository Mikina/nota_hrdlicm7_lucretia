function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move custom group to defined Z position, in spread across the map formation. Group is defined by table of unitIDs",
		parameterDefs = {
			{ 
				name = "groupDefintion",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "ZCoordinate",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "fight",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "false",
			}
		}
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

function dist ( x1, y1, x2, y2 )
	
	return math.sqrt ( math.pow ( x2 - x1, 2 ) + math.pow ( y2 - y1, 2 ) )
end

function dist_between ( nodeA, nodeB )

	return dist ( nodeA.x, nodeA.z, nodeB.x, nodeB.z )
end

function Run(self, units, parameter)
	local customGroup = parameter.groupDefintion -- table
  
	local ZCoordinate = parameter.ZCoordinate -- int
	local fight = parameter.fight -- boolean
  local position = Vec3(0,0,ZCoordinate)
	local formation = {}
  local spread = Game.mapSizeX / (customGroup["length"] + 2)
	for x=1, #customGroup do
    formation[#formation+1] = Vec3(x*spread,0,0)
    end
	--Spring.Echo(dump(parameter.formation))
	
	-- validation
	-- if (#units > #formation) then
		-- Logger.warn("formation.move", "Your formation size [" .. #formation .. "] is smaller than needed for given count of units [" .. #units .. "] in this group.") 
		-- return FAILURE
	-- end
	
	-- pick the spring command implementing the move
	local cmdID = CMD.MOVE
	if (fight) then cmdID = CMD.FIGHT end

	-- choose the pointmen
  pointmanID = customGroup[1]

	local pointX, pointY, pointZ = SpringGetUnitPosition(pointmanID)
	local pointmanPosition = Vec3(pointX, pointY, pointZ)
	

	-- check pointman success
	-- THIS LOGIC IS TEMPORARY, NOT CONSIDERING OTHER UNITS POSITION
	local pointmanOffset = formation[1]
	local pointmanWantedPosition = position + pointmanOffset
	if dist_between(pointmanPosition,pointmanWantedPosition) < 256 then
		return SUCCESS
  end
		
		for posIndex=1, customGroup["length"] do
      local unitID = customGroup[posIndex]
      local commands = Spring.GetUnitCommands(unitID)
      if #commands == 0 or commands[1]["id"] ~= CMD.LOAD_UNITS then
				local thisUnitWantedPosition = position + formation[posIndex]
				SpringGiveOrderToUnit(unitID, cmdID, {thisUnitWantedPosition.x,thisUnitWantedPosition.y,thisUnitWantedPosition.z}, {})
			end
		end
		
		return RUNNING
	end


function Reset(self)
end
