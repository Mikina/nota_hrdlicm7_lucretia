function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Follow given path.",
		parameterDefs = {

			{ 
				name = "path", -- relative formation
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "path",
			},
      { 
				name = "unit", -- relative formation
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "unit",
			}
		}
	}
end

function dist ( x1, y1, x2, y2 )
	
	return math.sqrt ( math.pow ( x2 - x1, 2 ) + math.pow ( y2 - y1, 2 ) )
end

function dist_between ( nodeA, nodeB )

	return dist ( nodeA.x, nodeA.z, nodeB.x, nodeB.z )
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
	self.lastPointmanPosition = Vec3(0,0,0)
  self.goal = nil
  self.unit = nil
  self.done = nil
end

function Run(self, units, parameter)
	local path = parameter.path
  local unit = parameter.unit-- Vec3
  if self.goal == nil then
    self.goal = path[#path]
  end
  if self.unit == nil then
    self.unit = unit
  end
  if self.lastPointmanPosition == nil then
    self.lastPointmanPosition = Vec3(0,0,0)
  end
  
  if unit == nil or Spring.GetUnitIsDead(self.unit)  then
    self.unit = nil
    return FAILURE
  end
  
  if self.done == nil then
  self.done = true
  local cmdID = CMD.MOVE
  for i=1, #path do
    local position = path[i]
      if position == nil then
        self.unit = nil
          return FAILURE
    end
    SpringGiveOrderToUnit(unit, cmdID, {position.x,position.y,position.z}, {"shift"})
  end
end
	
	-- pick the spring command implementing the move

  

  local pointX, pointY, pointZ = SpringGetUnitPosition(unit)
	local pointmanPosition = Vec3(pointX, pointY, pointZ)
  
  if (dist_between(pointmanPosition,self.lastPointmanPosition) < 10) then 
		self.threshold = self.threshold + THRESHOLD_STEP 
	else
		self.threshold = THRESHOLD_DEFAULT
	end
	self.lastPointmanPosition = pointmanPosition
  
  
    if dist_between(pointmanPosition,self.goal) < self.threshold then
      self.unit = nil
        return SUCCESS
    end
    


return RUNNING
end


function Reset(self)
	ClearState(self)
end
