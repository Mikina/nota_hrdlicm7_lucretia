function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Load the assigned unit.",
		parameterDefs = {

			{ 
				name = "unitToMove", -- relative formation
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
      			{ 
				name = "transport", -- relative formation
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
	self.lastPointmanPosition = Vec3(0,0,0)
  self.transport = nil
end

function Run(self, units, parameter)
	local unitToMove = parameter.unitToMove -- Vec3
  local transport = parameter.transport
  if self.transport == nil then
    self.transport = transport
  end
  -- validation
	if (unitToMove == nil) then
		Logger.warn("formation.move", "You need to give some units to load.") 
		return FAILURE
	end
	
  
  if transport == nil or Spring.GetUnitIsDead(self.transport)  then
    return FAILURE
  end
  
  if unitToMove == nil or Spring.GetUnitIsDead(unitToMove)  then
    self.transport = nil
    return SUCCESS
  end
  
	-- pick the spring command implementing the move
	local cmdID = CMD.LOAD_UNITS

	local pointX, pointY, pointZ = SpringGetUnitPosition(transport)
	local pointmanPosition = Vec3(pointX, pointY, pointZ)
	
	

		
      if Spring.GetUnitTransporter(unitToMove) == nil then
        local transportCommands = Spring.GetUnitCommands(transport)
        if transportCommands == nil or #transportCommands == 0 or transportCommands[1]["id"] ~= CMD.LOAD_UNITS then
         SpringGiveOrderToUnit(transport, cmdID, {unitToMove}, {})
         end
        local unitCommands = Spring.GetUnitCommands(unitToMove)
        if unitCommands == nil or #unitCommands == 0 or unitCommands[1]["id"] ~= CMD.LOAD_ONTO then
         SpringGiveOrderToUnit(unitToMove, CMD.LOAD_ONTO, {transport},{})
         end
      end
  
  if Spring.GetUnitIsTransporting(transport) ~= nil and #Spring.GetUnitIsTransporting(transport) == 1 then
    self.transport = nil
    return SUCCESS
  else
    return RUNNING
  end
end


function Reset(self)
	ClearState(self)
end
