function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move to defined position",
		parameterDefs = {
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "unitsToMove", -- relative formation
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "fight",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "true",
			}
		}
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
	self.lastPointmanPosition = Vec3(0,0,0)
end

function Run(self, units, parameter)
	local position = parameter.position -- Vec3
	local fight = parameter.fight -- boolean
  local unitsToMove = parameter.unitsToMove
	
	
	-- validation
	if (unitsToMove == nil or #unitsToMove == 0) then
		Logger.warn("formation.move", "You need to give some units to move.") 
		return FAILURE
	end
	
	-- pick the spring command implementing the move
	local cmdID = CMD.MOVE
	if (fight) then cmdID = CMD.FIGHT end

	local pointman = unitsToMove[1] -- while this is running, we know that #units > 0, so pointman is valid
	local pointX, pointY, pointZ = SpringGetUnitPosition(pointman)
	local pointmanPosition = Vec3(pointX, pointY, pointZ)
		if (pointmanPosition == self.lastPointmanPosition) then 
		self.threshold = self.threshold + THRESHOLD_STEP 
	else
		self.threshold = THRESHOLD_DEFAULT
	end
	self.lastPointmanPosition = pointmanPosition
	
	-- check pointman success
	-- THIS LOGIC IS TEMPORARY, NOT CONSIDERING OTHER UNITS POSITION
	if (pointmanPosition:Distance(position) < self.threshold) then
		return SUCCESS
	else
	
		SpringGiveOrderToUnit(pointman, cmdID, position:AsSpringVector(), {})
		local alternate = 1
    local x = 1
		for i=2, #unitsToMove do
       alternate = -alternate
       if alternate == 1 then
         x = x+1
         end
			local thisUnitWantedPosition = pointmanPosition + Vec3(5*x,0,5*alternate*i)
			SpringGiveOrderToUnit(unitsToMove[i], cmdID, thisUnitWantedPosition:AsSpringVector(), {})
		
		
		
  end
  return RUNNING
  end
end


function Reset(self)
	ClearState(self)
end
