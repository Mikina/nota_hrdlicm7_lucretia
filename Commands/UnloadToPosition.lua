function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Unload units to position.",
		parameterDefs = {

			{ 
				name = "position", -- relative formation
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
      			{ 
				name = "transport", -- relative formation
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
	self.lastPointmanPosition = Vec3(0,0,0)
end

function Run(self, units, parameter)
	local position = parameter.position -- Vec3
  repeat
	local unitsInCylinder = Spring.GetUnitsInCylinder(position.x, position.z, 64)
  if #unitsInCylinder > 2 then
    position = position + Vec3(0,0,64)
  end
  until #unitsInCylinder < 3
	-- pick the spring command implementing the move
	local cmdID = CMD.UNLOAD_UNITS

	local transport = parameter.transport -- while this is running, we know that #units > 0, so pointman is valid
	SpringGiveOrderToUnit(transport, cmdID, {position["x"],position["y"],position["z"],0}, {})

  if #Spring.GetUnitIsTransporting(transport) == 0 then
    return SUCCESS
  else
    return RUNNING
    end
end


function Reset(self)
	ClearState(self)
end
