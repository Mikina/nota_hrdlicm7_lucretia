function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Load the assigned units.",
		parameterDefs = {

			{ 
				name = "unitsToMove", -- relative formation
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
	self.lastPointmanPosition = Vec3(0,0,0)
end

function Run(self, units, parameter)
	local unitsToMove = parameter.unitsToMove -- Vec3
  -- validation
	if (unitsToMove == nil or #unitsToMove == 0) then
		Logger.warn("formation.move", "You need to give some units to load.") 
		return FAILURE
	end
	
	-- pick the spring command implementing the move
	local cmdID = CMD.LOAD_UNITS

	local transport = units[1] -- while this is running, we know that #units > 0, so pointman is valid
	local pointX, pointY, pointZ = SpringGetUnitPosition(transport)
	local pointmanPosition = Vec3(pointX, pointY, pointZ)
	
	

		for i=1, #unitsToMove do
      if Spring.GetUnitTransporter(unitsToMove[i]) == nil then
			SpringGiveOrderToUnit(transport, cmdID, {unitsToMove[i]}, {})
      SpringGiveOrderToUnit(unitsToMove[i], CMD.LOAD_ONTO, {transport},{})
      end
  end
  if #Spring.GetUnitIsTransporting(transport) == #unitsToMove then
    return SUCCESS
  else
    return RUNNING
    end
end


function Reset(self)
	ClearState(self)
end
