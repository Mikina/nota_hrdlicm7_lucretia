function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Stops the unit.",
		parameterDefs = {

			{ 
				name = "unitID", -- relative formation
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- constants
local THRESHOLD_DEFAULT = 10

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
  self.lastPos = nil
end

function Run(self, units, parameter)
	local unit = parameter.unitID -- Vec3
  local position = Vec3(SpringGetUnitPosition(unit))
  if self.lastPos ~= nil and position == self.lastPos then
    return SUCCESS
  end
  
  SpringGiveOrderToUnit(unit, CMD.STOP, {}, {})
  SpringGiveOrderToUnit(unit, CMD.MOVE, {position["x"],position["y"],position["z"],radius}, {})
  for i=1,10 do
    SpringGiveOrderToUnit(unit, CMD.MOVE, {position["x"],position["y"],position["z"],radius}, {"shift"})
  end
  local _,_,_,len = Spring.GetUnitVelocity(unit)
 self.lastPos = position
return RUNNING

end


function Reset(self)
	ClearState(self)
end
