function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Unload units to area.",
		parameterDefs = {

			{ 
				name = "position", -- relative formation
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
      			{ 
				name = "areaRadius", -- relative formation
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
      			{ 
				name = "transport", -- relative formation
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- constants
local THRESHOLD_DEFAULT = 30
-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
	self.lastPointmanPosition = Vec3(0,0,0)
  self.unloadTries = 0
end

function Run(self, units, parameter)
	local position = parameter.position -- Vec3
  local radius = parameter.areaRadius
	if self.unloadTries == nil then
    self.unloadTries = THRESHOLD_DEFAULT
  end
	-- pick the spring command implementing the move
	local cmdID = CMD.UNLOAD_UNITS
  self.unloadTries = self.unloadTries + 1
	local transport = parameter.transport -- while this is running, we know that #units > 0, so pointman is valid
  if transport == nil or Spring.GetUnitIsDead(transport)  then
    return FAILURE
  end
  local transportCommands = Spring.GetUnitCommands(transport)

  if self.unloadTries > THRESHOLD_DEFAULT and (transportCommands == nil or #transportCommands == 0) then
  SpringGiveOrderToUnit(transport, cmdID, {position["x"],position["y"],position["z"],radius}, {})
  end
  if Spring.GetUnitIsTransporting(transport) == nil or #Spring.GetUnitIsTransporting(transport) == 0 then
    return SUCCESS
  else
    return RUNNING
    end
end


function Reset(self)
	ClearState(self)
end
