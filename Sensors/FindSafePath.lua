local sensorInfo = {
	name = "FindSafePath",
	desc = "Finds safe path between A and B.",
	author = "hrdlicm7",
	date = "2019-05-8",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching
local mapCache = {}
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function GetEnemyWeaponRange(enemyID)
  local enemyDefID = Spring.GetUnitDefID(enemyID)
  if enemyDefID == nil then
    return 1200
  end
	local enemyWeapons =  UnitDefs[enemyDefID].weapons
  local range = 0
  for _,enemyWeapon in pairs(enemyWeapons) do
    local enemyWeaponDefID = enemyWeapon.weaponDef
    local weapon = WeaponDefs[enemyWeaponDefID]
    if weapon.range > range then
      range = weapon.range
    end
  end
	return range
end
    
    
function IsInEnemyRange(position,enemyID)
  x,y,z = Spring.GetUnitPosition(enemyID)
  enemyPos = Vec3(x,y,z)
  if position["y"] < enemyPos["y"] - 500 then
    return false
  end
  
  if position:Distance(enemyPos) > GetEnemyWeaponRange(enemyID) + 100 then
    return false
  end
  return true
end

function GetNearbyEnemyUnits(position)
  local nearbyUnits = Spring.GetUnitsInSphere(position["x"], position["y"], position["z"], 1300)
  local enemyUnits = {}
  local _,_,_,myTeamID = Spring.GetPlayerInfo(0)
  for _,unit in pairs(nearbyUnits) do
    if Spring.GetUnitTeam(unit) ~= myTeamID then
      enemyUnits[#enemyUnits + 1] = unit
    end
  end
  return enemyUnits
end

function IsOnMap(position)
  return position.x > 0 and position.x < Game.mapSizeX and position.z > 0 and position.z < Game.mapSizeZ
end

function IsPositionSafe(position, graph)

  if graph[position.x] == nil then
    graph[position.x] = {}
  end
  if graph[position.x][position.z] == nil then
    Logger.warn("findSafePath", "Map not cached properly!!" .. position.x .. " : " .. position.z)
    graph[position.x][position.z] = CalculatePositionSafe(position)
  end
  return graph[position.x][position.z]
end

function CalculatePositionSafe(position)
  if position.y > 750 or not IsOnMap(position) then
    return false
  end
    
  local enemyUnits = GetNearbyEnemyUnits(position)
  for _,unit in pairs(enemyUnits) do
  if IsInEnemyRange(position, unit) then
    return false
  end
  end
  return true
end
-- AStar Lua implementation from https://github.com/lattejed/a-star-lua    
-- ======================================================================
-- Copyright (c) 2012 RapidFire Studio Limited 
-- All Rights Reserved. 
-- http://www.rapidfirestudio.com

-- Permission is hereby granted, free of charge, to any person obtaining
-- a copy of this software and associated documentation files (the
-- "Software"), to deal in the Software without restriction, including
-- without limitation the rights to use, copy, modify, merge, publish,
-- distribute, sublicense, and/or sell copies of the Software, and to
-- permit persons to whom the Software is furnished to do so, subject to
-- the following conditions:

-- The above copyright notice and this permission notice shall be
-- included in all copies or substantial portions of the Software.

-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
-- EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
-- MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
-- IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
-- CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
-- TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
-- SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
-- ======================================================================



----------------------------------------------------------------
-- local variables
----------------------------------------------------------------

local INF = 1/0
local cachedPaths = nil
local SAMPLE_RESOLUTION = 128
----------------------------------------------------------------
-- local functions
----------------------------------------------------------------

function dist ( x1, y1, x2, y2 )
	
	return math.sqrt ( math.pow ( x2 - x1, 2 ) + math.pow ( y2 - y1, 2 ) )
end

function dist_between ( nodeA, nodeB )

	return dist ( nodeA.x, nodeA.z, nodeB.x, nodeB.z )
end

function heuristic_cost_estimate ( nodeA, nodeB )

	return dist ( nodeA.x, nodeA.z, nodeB.x, nodeB.z )
end

function is_valid_node ( node, neighbor )

	return true
end

function lowest_f_score ( set, f_score )

	local lowest, bestNode = INF, nil
	for _, node in ipairs ( set ) do
		local score = f_score [ node.x ][node.z]
		if score < lowest then
			lowest, bestNode = score, node
		end
	end
	return bestNode
end

function neighbor_nodes ( theNode, nodes, graph )

	
	local N = Vec3(theNode.x,-1,theNode.z + SAMPLE_RESOLUTION)
  local NE = Vec3(theNode.x + SAMPLE_RESOLUTION,-1,theNode.z + SAMPLE_RESOLUTION)
  local E = Vec3(theNode.x + SAMPLE_RESOLUTION,-1,theNode.z)
  local SE = Vec3(theNode.x + SAMPLE_RESOLUTION,-1,theNode.z - SAMPLE_RESOLUTION)
  local S = Vec3(theNode.x,-1,theNode.z - SAMPLE_RESOLUTION)
  local SW = Vec3(theNode.x - SAMPLE_RESOLUTION,-1,theNode.z - SAMPLE_RESOLUTION)
  local W = Vec3(theNode.x - SAMPLE_RESOLUTION,-1,theNode.z)
  local NW = Vec3(theNode.x - SAMPLE_RESOLUTION,-1,theNode.z + SAMPLE_RESOLUTION)
  local neighbors = {N,NE,E,SE,S,SW,W,NW}
  local valid_neighbors = {}
  for _, neighbor in pairs(neighbors) do
    neighbor.y = Spring.GetGroundHeight(neighbor.x,neighbor.z)
    if IsPositionSafe(neighbor, graph) then
      valid_neighbors[#valid_neighbors + 1] = neighbor
    end
  end
	return valid_neighbors
end

function not_in ( set, theNode )

	for _, node in ipairs ( set ) do
		if node == theNode then return false end
	end
	return true
end

function compare(a, b)
  
  return a.x == b.x and a.z == b.z
end

function remove_node ( set, theNode )

	for i, node in ipairs ( set ) do
		if compare(node,theNode) then 
			set [ i ] = set [ #set ]
			set [ #set ] = nil
			break
		end
	end	
end

function unwind_path ( flat_path, map, current_node )

	if map [ current_node.x ] [ current_node.z ] then
		table.insert ( flat_path, 1, map [ current_node.x ][ current_node.z ] ) 
		return unwind_path ( flat_path, map, map [ current_node.x ][ current_node.z ] )
	else
		return flat_path
	end
end



----------------------------------------------------------------
-- pathfinding functions
----------------------------------------------------------------

function roundToSample(vector)
  vector.x = vector.x - vector.x % SAMPLE_RESOLUTION
  vector.z = vector.z - vector.z % SAMPLE_RESOLUTION
  vector.y = Spring.GetGroundHeight(vector.x,vector.z)
  return vector
end

function a_star ( start, goal, nodes, valid_node_func, graph)
  start = roundToSample(start)
  goal = roundToSample(goal)
	local closed = {}
  local open = {}
  local openSet = {}
  local g_score, f_score = {}, {}
	local came_from = {}
  for x=0, Game.mapSizeX, SAMPLE_RESOLUTION do
    closed[x] = {}
    open[x] = {}
    g_score[x] = {}
    f_score[x] = {}
    came_from[x] = {}
    for z=0, Game.mapSizeZ, SAMPLE_RESOLUTION do
      closed[x][z] = false
      open[x][z] = false
      g_score[x][z] = Game.mapSizeX * Game.mapSizeZ
      f_score[x][z] = Game.mapSizeX * Game.mapSizeZ
    end
  end
  
	open[start.x][start.z] = true
  openSet[#openSet + 1] = start
  local openCount = 1

	if valid_node_func then is_valid_node = valid_node_func end

	
	g_score [start.x][start.z] = 0
	f_score [start.x][start.z] = g_score [start.x][start.z] + heuristic_cost_estimate ( start, goal )

	while #openSet > 0 do
	
		local current = lowest_f_score (openSet, f_score )
		if compare(current, goal) then
			local path = unwind_path ( {}, came_from, goal )
			table.insert ( path, goal )
			return path
		end

		open[current.x][current.z] = false
    remove_node(openSet,current)
		closed[current.x][current.z] = true	
		
		local neighbors = neighbor_nodes ( current, nodes, graph )
		for _, neighbor in ipairs ( neighbors ) do 
			if not closed[neighbor.x][neighbor.z] then
			
				local tentative_g_score = g_score [current.x][current.z] + dist_between ( current, neighbor )
				 
				if not open[neighbor.x][neighbor.z] or tentative_g_score < g_score [neighbor.x][neighbor.z] then 
					came_from 	[neighbor.x][neighbor.z] = current
					g_score 	[neighbor.x][neighbor.z] = tentative_g_score
					f_score 	[neighbor.x][neighbor.z] = g_score [neighbor.x][neighbor.z] + heuristic_cost_estimate ( neighbor, goal )
					if not open[neighbor.x][neighbor.z] then
						open[neighbor.x][neighbor.z] = true
            openSet[#openSet + 1] = neighbor
					end
				end
			end
		end
	end
	return nil -- no valid path
end

----------------------------------------------------------------
-- exposed functions
----------------------------------------------------------------

function clear_cached_paths ()

	cachedPaths = nil
end

function distance ( x1, y1, x2, y2 )
	
	return dist ( x1, y1, x2, y2 )
end

function path ( start, goal, nodes, ignore_cache, valid_node_func, graph)

	if not cachedPaths then cachedPaths = {} end
	if not cachedPaths [ start ] then
		cachedPaths [ start ] = {}
	elseif cachedPaths [ start ] [ goal ] and not ignore_cache then
		return cachedPaths [ start ] [ goal ]
	end

      local resPath = a_star ( start, goal, nodes, valid_node_func, graph )
      if not cachedPaths [ start ] [ goal ] and not ignore_cache then
              cachedPaths [ start ] [ goal ] = resPath
      end

	return resPath
end

----------------------------------------------------------------
-- NOTA Function Code
----------------------------------------------------------------

-- speedups
local SpringGetWind = Spring.GetWind
-- @description return current wind statistics
return function(start,goal, oldCache)
  if oldCache == nil then
    oldCache = {}
  end
  
  mapCache = oldCache
	return {["path"] = path(start,goal, nil, false, nil, mapCache), ["map"] = mapCache}
end

