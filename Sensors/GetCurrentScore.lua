local sensorInfo = {
	name = "GetCurrentScore",
	desc = "Gets current score from Mission info",
	author = "PepeAmpere",
	date = "2018-04-16",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching


function mysplit(inputstr, sep)
        if sep == nil then
                sep = "%s"
        end
        local t={}
        for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
                table.insert(t, str)
        end
        return t
end

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return current wind statistics
return function(area)
	local info = Spring.GetGameRulesParam("MissionInfo")
  local parsedInfo = mysplit(info,"|")
  for _, rule in pairs(parsedInfo) do
    local name = mysplit(rule,"=")[1]
    if name == "score" then
    local value = mysplit(rule,"#")[2]
    return tonumber(value)
    end
  end
end