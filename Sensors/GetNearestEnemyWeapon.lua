local sensorInfo = {
	name = "GetNearestEnemyWeapon",
	desc = "Sends data to example debug widget",
	author = "PepeAmpere",
	date = "2018-04-16",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return current wind statistics
return function()
	local nearestEnemyID = Spring.GetUnitNearestEnemy(units[1])
	if nearestEnemyID == nil then
	 return {}
	 end
	local enemyDefID = Spring.GetUnitDefID(nearestEnemyID)
	local enemyWeapons =  UnitDefs[enemyDefID].weapons[1]
	if enemyWeapons == nil then
	return {}
	end
	local enemyWeaponDefID = enemyWeapons.weaponDef
	local enemyName = UnitDefs[enemyDefID].name
	local weapon = WeaponDefs[enemyWeaponDefID]
	local toReturn = {["name"] = enemyName, ["weaponName"] = weapon.name, ["weaponRange"] = weapon.range}
	return toReturn;
	
end