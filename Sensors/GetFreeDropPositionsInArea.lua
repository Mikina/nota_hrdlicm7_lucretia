local sensorInfo = {
	name = "Get map hills",
	desc = "Return sampled points with maximal height on map",
	author = "JanHolan",
	date = "2019-07-24",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(areaInfo)
	local area = areaInfo;
	local minX = area.center.x - area.radius;
	local maxX = area.center.x + area.radius;
	local minZ = area.center.z - area.radius;
	local maxZ = area.center.z + area.radius;
	local freeSpotRadius = 128;
	local positions = {};

	for x = minX, maxX, freeSpotRadius do
		for z = minZ, maxZ, freeSpotRadius do
			local positionHeight = Spring.GetGroundHeight(x, z);
			local testedPosition = Vec3(x, positionHeight, z);
			local distance = math.sqrt((testedPosition.x - area.center.x) * (testedPosition.x - area.center.x) + (testedPosition.z - area.center.z) * (testedPosition.z - area.center.z));

			if (distance < area.radius) then
				local unitsInCylinder = Spring.GetUnitsInCylinder(testedPosition.x, testedPosition.z, freeSpotRadius);
				if (#unitsInCylinder == 0) then
					positions[#positions + 1] = testedPosition;
				end
			end
		end
	end
	return positions;
end