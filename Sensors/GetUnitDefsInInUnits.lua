local sensorInfo = {
	name = "GetUnitDefsInUnits",
	desc = "Get a List of UnitDefs in units.",
	author = "Hrdlicm7",
	date = "2019-05-13",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching
function tableContains(table, data)
  if table == nil then
    return false
  end
  for _, element in pairs(table) do
    if element == data then
      return true
    end
  end
  
  return false
end
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetWind = Spring.GetWind

-- @description return current wind statistics
return function()
  local unitDefs = {}
  Spring.Echo(dump(oneUnit))
	for i=1,units["length"] do
    local unitDefID = Spring.GetUnitDefID(units[i])
    if not tableContains(unitDefs, unitDefID) then
      unitDefs[#unitDefs+1] = unitDefID
    end
  end
  return unitDefs
end