local sensorInfo = {
	name = "findHillCoordinates",
	desc = "Returns center coordinates of all hills on map.",
	author = "hrdlicm7",
	date = "2018-04-22",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

--speedup
local SpringGetGroundHeight = Spring.GetGroundHeight

-- caching
local hillCoordinates
local sampleResolution = 64;
local toleranceMargin = 5;

function isFound(x,z,foundHills)
  for i, hill in ipairs(foundHills) do
  if x >= hill["fromX"] and x <= hill["toX"] and z >= hill["fromZ"] and z <= hill["toZ"] then
    return true
    end
end
  end

function doesItFit(coords,minHeight)
  return SpringGetGroundHeight(coords["x"],coords["z"]) >= minHeight
  end

function getHeightRectangle(x,z,minHeight)
  local boundingBox = { ["fromX"] = x, ["fromZ"] = z, ["toX"] = x, ["toZ"] = z }
  local queue = {}
  local lastIndex = 0;
  lastIndex = lastIndex + 1
  queue[lastIndex] = {["x"] = x, ["z"] = z}
  found = {[x .. ":" .. z] = 1}
  while lastIndex ~= 0 do
    local current = queue[lastIndex]
    lastIndex = lastIndex - 1
    
    directions = {sampleResolution/2, 0, -sampleResolution/2}
    
    for i,dX in ipairs(directions) do
      for j,dZ in ipairs(directions) do
        local nearby = {["x"] = current["x"] + dX, ["z"] = current["z"] + dZ}
        if found[nearby["x"] .. ":" .. nearby["z"]] == nil and doesItFit(nearby, minHeight) then
          found[nearby["x"] .. ":" .. nearby["z"]] = 1
          lastIndex = lastIndex + 1
          queue[lastIndex] = {["x"] = nearby["x"], ["z"] = nearby["z"]}
          
          if nearby["x"] < boundingBox["fromX"] then
             boundingBox["fromX"] = nearby["x"]
           end
           
          if nearby["x"] > boundingBox["toX"] then
             boundingBox["toX"] = nearby["x"]
           end
           
          if nearby["z"] < boundingBox["fromZ"] then
             boundingBox["fromZ"] = nearby["z"]
           end 
           
           if nearby["z"] > boundingBox["toZ"] then
             boundingBox["toZ"] = nearby["z"]
           end
        end
      end
    end
  end
  local center = {["x"] = boundingBox["fromX"] + ((boundingBox["toX"] - boundingBox["fromX"] )/2), ["z"] = boundingBox["fromZ"] + ((boundingBox["toZ"] - boundingBox["fromZ"] )/2)}
  boundingBox["center"] = Vec3(center["x"], 0, center["z"])
  return boundingBox
end

function computeHills()
    local minHeight, maxHeight = Spring.GetGroundExtremes()
    local toleranceHeight = maxHeight - ((maxHeight - minHeight) / toleranceMargin)
    local foundHills = {}
    for x=0, Game.mapSizeX, sampleResolution do
      for z=0, Game.mapSizeZ, sampleResolution do
        if not isFound(x,z,foundHills) then
        height = SpringGetGroundHeight(x,z)
        
        if height > toleranceHeight then
          foundHills[#foundHills+1] = getHeightRectangle(x,z,toleranceHeight)
        end
        end
      
      end
    end
    
    return foundHills
end


-- @description return array of hill centers.
return function(tolerance)
	if hillCoordinates == nil then
    hillCoordinates = computeHills()
  end
  
  return hillCoordinates;
end



