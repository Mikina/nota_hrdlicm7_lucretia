local sensorInfo = {
	name = "CalculateMap",
	desc = "Creates a full Map Graph.",
	author = "hrdlicm7",
	date = "2019-04-16",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching
local SAMPLE_RESOLUTION = 128
function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function GetEnemyWeaponRange(enemyID)
  local enemyDefID = Spring.GetUnitDefID(enemyID)
  if enemyDefID == nil then
    return 1200
  end
	local enemyWeapons =  UnitDefs[enemyDefID].weapons
  local range = 0
  for _,enemyWeapon in pairs(enemyWeapons) do
    local enemyWeaponDefID = enemyWeapon.weaponDef
    local weapon = WeaponDefs[enemyWeaponDefID]
    if weapon.range > range then
      range = weapon.range
    end
  end
	return range
end
    
    
function IsInEnemyRange(position,enemyID)
  x,y,z = Spring.GetUnitPosition(enemyID)
  enemyPos = Vec3(x,y,z)
  if position["y"] < enemyPos["y"] - 400 then
    return false
  end
  
  if position:Distance(enemyPos) > GetEnemyWeaponRange(enemyID) + 100 then
    return false
  end
  return true
end

function GetNearbyEnemyUnits(position)
  local nearbyUnits = Spring.GetUnitsInSphere(position["x"], position["y"], position["z"], 1300)
  local enemyUnits = {}
  local _,_,_,myTeamID = Spring.GetPlayerInfo(0)
  for _,unit in pairs(nearbyUnits) do
    if Spring.GetUnitTeam(unit) ~= myTeamID then
      enemyUnits[#enemyUnits + 1] = unit
    end
  end
  return enemyUnits
end

function IsOnMap(position)
  return position.x > 0 and position.x < Game.mapSizeX and position.z > 0 and position.z < Game.mapSizeZ
end

function IsPositionSafe(position)

  if mapCache[position.x] == nil then
    mapCache[position.x] = {}
  end
  if mapCache[position.x][position.z] == nil then
    mapCache[position.x][position.z] = CalculatePositionSafe(position)
  end
  return mapCache[position.x][position.z]
end

function CalculatePositionSafe(position)
  if position.y > 700 or not IsOnMap(position) then
    return false
  end
    
  local enemyUnits = GetNearbyEnemyUnits(position)
  for _,unit in pairs(enemyUnits) do
  if IsInEnemyRange(position, unit) then
    return false
  end
  end
  return true
end
-- @description return current wind statistics
return function()
  local mapCache = {}
    for x=-SAMPLE_RESOLUTION, Game.mapSizeX+SAMPLE_RESOLUTION, SAMPLE_RESOLUTION do
    mapCache[x] = {}
    for z=-SAMPLE_RESOLUTION, Game.mapSizeZ+SAMPLE_RESOLUTION, SAMPLE_RESOLUTION do
      mapCache[x][z] = CalculatePositionSafe(Vec3(x,Spring.GetGroundHeight(x,z),z))
    end
  end
	return mapCache
end