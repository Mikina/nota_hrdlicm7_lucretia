local sensorInfo = {
  name = "windPositionBehindCommander",
  desc = "Returns a position behind the given unit, opposite of current wind.",
  author = "hrdlicm7",
  date = "2019-04-16",
  license = "CC0",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
  return {
    period = EVAL_PERIOD_DEFAULT 
  }
end

-- speedups
local SpringGetWind = Spring.GetWind
local SpringGetUnitPosition = Spring.GetUnitPosition
-- @description return point behind a commander, against wind.
return function(commander)
  local pointX, pointY, pointZ = SpringGetUnitPosition(commander)
  local dirX, dirY, dirZ, strength, normDirX, normDirY, normDirZ = SpringGetWind()
  
  return Vec3(pointX - dirX * 5, pointY - dirY * 5, pointZ - dirZ * 5)

end