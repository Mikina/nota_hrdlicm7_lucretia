local sensorInfo = {
  name = "SwarmFormation",
  desc = "Returns array of relative positions for swarm formation.",
  author = "hrdlicm7",
  date = "2019-04-16",
  license = "CC0",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
  return {
    period = EVAL_PERIOD_DEFAULT 
  }
end


-- @description return swarm formation relative position array.
return function()
  local posArray = { -- position array shamelessly stolen from formation.Definition() Sensor.
    [1]  = {0,0},		[2]  = {9,-1},		[3]  = {2,-8},		[4]  = {-5,-7},		[5]  = {-10,4},
    [6]  = {1,10},		[7]  = {12,9},		[8]  = {16,-2},		[9]  = {12,-11},	[10] = {1,-17},
    [11] = {-8,-16},	[12] = {-15,-3},	[13] = {-15,10},	[14] = {-5,18},		[15] = {8,19},
    [16] = {21,13},		[17] = {25,2},		[18] = {21,-10},	[19] = {6,-20},		[20] = {-4,-22},
    [21] = {-17,-7},	[22] = {-22,2},		[23] = {-15,20},	[24] = {3,26},		[25] = {18,23},
    [26] = {29,10},		[27] = {28,-7},		[28] = {21,-20},	[29] = {5,-27},		[30] = {-13,-24},
  }
  local newPositions = {}
  for i=1, #posArray do
     newPositions[#newPositions+1] = Vec3(posArray[i][1]*3,0,posArray[i][2]*3)
  end
  return newPositions

end