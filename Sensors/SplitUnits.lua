local sensorInfo = {
	name = "splitUnits",
	desc = "Splits units from index and count",
	author = "hrdlicm7",
	date = "2019-04-25",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return current wind statistics
return function(fromIndex, count)
  local newUnits = {}
	if #units >= count + fromIndex then
    if count <= 0 then
      count = #units - fromIndex
    end
		for i=0,count do
      newUnits[#newUnits + 1] = units[fromIndex + i]
    end
	end
  return newUnits
end