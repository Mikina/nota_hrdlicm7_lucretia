local sensorInfo = {
	name = "Get Evac Unit Queue",
	desc = "Creates a queue with units to evacuate, based on distance.",
	author = "Hrdlicm7",
	date = "2019-05-13",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function dist ( x1, y1, x2, y2 )
	
	return math.sqrt ( math.pow ( x2 - x1, 2 ) + math.pow ( y2 - y1, 2 ) )
end

function dist_between ( nodeA, nodeB )

	return dist ( nodeA.x, nodeA.z, nodeB.x, nodeB.z )
end

function bubbleSort(A, evacPoint)
  local itemCount=#A
  local hasChanged
  repeat
    hasChanged = false
    itemCount=itemCount - 1
    for i = 1, itemCount do
      if dist_between(evacPoint, Vec3(Spring.GetUnitPosition(A[i]))) > dist_between(evacPoint, Vec3(Spring.GetUnitPosition(A[i+1]))) then
        A[i], A[i + 1] = A[i + 1], A[i]
        hasChanged = true
      end
    end
  until hasChanged == false
  return A
end

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

function tableContains(table, data)
  if table == nil then
    return false
  end
  for _, element in pairs(table) do
    if element == data then
      return true
    end
  end
  
  return false
end

function IsInZone(position, zone)
  return zone ~= nil and dist_between(position, zone["center"]) < zone["radius"]
end
-- @description return current wind statistics
return function(evacPoint, ignoreDefID, ignoreZone)
	local allUnits = Spring.GetTeamUnits(Spring.GetLocalTeamID())
  local interestingUnits = {}
  for _, unit in pairs(allUnits) do
    local pos = Vec3(Spring.GetUnitPosition(unit))
    if not tableContains(ignoreDefID, Spring.GetUnitDefID(unit)) and not IsInZone(pos,ignoreZone) and not IsInZone(pos,evacPoint) then
      interestingUnits[#interestingUnits+1] = unit
    end
  end
	return bubbleSort(interestingUnits,evacPoint["center"])
end