local sensorInfo = {
	name = "scaledWindDirection",
	desc = "Returns flattened wind direction.",
	author = "hrdlicm7",
	date = "2018-04-16",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speedups
local SpringGetWind = Spring.GetWind

-- @description return current wind statistics
return function(scale)
	local dirX, dirY, dirZ, strength, normDirX, normDirY, normDirZ = SpringGetWind()
	return {
		x = scale * dirX,
		z = scale * dirZ
	}
end